using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Puntaje : MonoBehaviour
{
    private int puntos;
    private int multiplicador;
    private int puntajeAlmacenado;
    private TextMeshProUGUI textMesh;
    public int puntajeNecesario;
    private void Start()
    {
        textMesh = GetComponent<TextMeshProUGUI>();
    }//Iniciar texto a cada nivel
    private void Update()
    {
        textMesh.text = puntos.ToString("Score : 0");
    } //Modificar el texto
    public void SumatoriaPuntos(int puntosEntrada)
    {
        puntos += puntosEntrada;
        if(puntos >= puntajeNecesario)
        {
            SceneManager.LoadScene("Win");
        }
    } //Sumar puntos
}

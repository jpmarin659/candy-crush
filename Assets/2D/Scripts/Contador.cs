using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Contador : MonoBehaviour
{
    public int minutos;
    public int seg;
    public TMP_Text tmp_Text;
    private float restanrtes;
    public bool enMarcha;

    private void Awake()
    {
        restanrtes = (minutos * 60) + seg;
    } //Tiempo al empezar el nivel
    void Update()
    {
        if(enMarcha)
        {
            restanrtes -= Time.deltaTime;
            if(restanrtes < 1)
            {
                SceneManager.LoadScene("Game Over");
            }
            int tempMin = Mathf.FloorToInt(restanrtes / 60);
            int tempSeg = Mathf.FloorToInt(restanrtes % 60);

            tmp_Text.text = string.Format("{00:00} : {01:00}", tempMin, tempSeg);
        }
    } //Divicion y de minutos y segundos
}

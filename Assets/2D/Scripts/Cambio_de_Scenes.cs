using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Cambio_de_Scenes : MonoBehaviour
{
    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    } //menu
    public void Reglas()
    {
        SceneManager.LoadScene("Reglas");
    } //reglas
    public void Juego()
    {
        SceneManager.LoadScene("Juego");
    } //juego
    public void Juego1()
    {
        SceneManager.LoadScene("Juego 1");
    }  //juego
    public void Juego2()
    {
        SceneManager.LoadScene("Juego 2");
    }  //juego
    public void Juego3()
    {
        SceneManager.LoadScene("Juego 3");
    }  //juego
    public void Juego4()
    {
        SceneManager.LoadScene("Juego 4");
    } //juego
    public void Win()
    {
        SceneManager.LoadScene("Win");
    }//ganar
    public  void Nievels()
    {
        SceneManager.LoadScene("Niveles");
    }//niveles
    public void GameOver()
    {
        SceneManager.LoadScene("Game Over");
    }//perder
    public void Nivele()
    {
        SceneManager.LoadScene("Niveles");
    }//niveles
    public void Salir()
    {
        Application.Quit();
    }//salir
}

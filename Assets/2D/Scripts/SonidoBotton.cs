using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonidoBotton : MonoBehaviour
{
    public AudioSource source;
    public AudioClip hoverFx;
    public AudioClip clickFx;

    public void HoverSound()
    {
        source.PlayOneShot(hoverFx);
    } //Detecta el mouse
    public void ClickSound()
    {
        source.PlayOneShot(clickFx);
    } //Click del mouse
}

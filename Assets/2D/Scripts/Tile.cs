using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public int xIndex;
    public int yIndex;

    Board m_board;


    public void Init(int cambioX, int cambioY, Board board)
    {
        xIndex = cambioX;
        yIndex = cambioY;

        m_board = board;
    } //Movimientos de la fichas

    public void OnMouseDown()
    {
        m_board.ClickedTile(this);
    } //Detectar el mouse

    public void OnMouseEnter()
    {
        m_board.DragToTile(this);
    } //Cliek del mouse

    public void OnMouseUp()
    {
        m_board.ReleaseTile();
    } //Guardar ficha con el click
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static int Score = 0;
    public string scoreStrings = "Score";
    public Text textScore;
    public GameController gameController;

    public void Awake()
    {
        gameController = this; 
    }

    void Start()
    {
        Score = 0;
    }

    void Update()
    {
        if (textScore != null)
        {
            textScore.text = scoreStrings + Score.ToString();
        }
    }
}
